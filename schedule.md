# Echéances

## Examens

### BDD
* type: qcm
* lieu: online
* date: 14/12/2022 au 10/01/2022
* src: BBB dernier cours

### Gestion projet
* type: ? ( pas d'exam ? )
* lieu: ?
* date: ?   
* src: ?

### Javascript
* type: pas exam -> project
* lieu: -
* date: -
* src: -

### Html 
* type: qcm
* lieu: online
* date: 14/12/2022 au 10/01/2022
* src: mail EP 23 : Dernière séance du 14 décembre et évaluation

### App Mobile
* type: qcm
* lieu: online
* date: 14/12/2022 au 10/01/2022
* src: ?

### Php
* type: papier
* lieu: presentiel avec la convocation
* date: 11/01/2022 14h
* src: BBB derniers cours
* remarques: Support de cours + notes + code papier permis

### Cms
* type: papier
* lieu: presentiel avec la convocation ( indique web dynamique pour cms ? )
* date: 11/01/2022 14h
* src: BBB dernier cours 
* remarques: examens avec peux de css, différent des examens années prédédentes. Support de cours + notes + code papier permis

## Projets 

### Javascript
* sujet: mini site deux pages jeux Olympiques
* group: non
* consignes: [https://moodle.univ-lyon2.fr/pluginfile.php/680645/mod_resource/content/1/ProjetJavaScript_2021_2022.pdf](moodle)
* date de remise: 09/01/2022


### Html
* sujet: Rendu Page HTML
* group: non ( voir 2min question Arnaud [https://scalelite-peda.univ-lyon2.fr/playback/presentation/2.3/7e10efe3521656c566e67b11f23f47e602bf4644-1637048442113?meetingId=7e10efe3521656c566e67b11f23f47e602bf4644-1637048442113](BBB) )
* consignes: [https://moodle.univ-lyon2.fr/mod/assign/view.php?id=43939](moodle)
* date de remise: 07/01/2022

### App Mobile
* sujet: creation d'une application ios via xcode
* group: non
* consignes:  ?
* date de remise: ?
* remarque: envoyer un mail avec sa proposition de réalisation à Serge Miguet: serge.miguet@univ-lyon2.fr avant le ?

### Gestion projet
* sujet: ?
* group: oui
* consignes: ? 
* date de remise: ? 
    
### Projet fin d'année 
* sujet: ?
* group: oui
* consignes: ? 
* date de remise: ? 
