# Javascript 

##  Debug
* console dans le navigateur
* Ctrl+Shift+I -> open console

## String
"chaine de charactéres"   
'chaine de charactéres'  
"chaine de charactéres avec 'sous' chaine incluse"   
"chaine de charactéres avec \"sous\" chaine incluse"  

### Echapement
\" == "   
\' == '  
\\ == \  
\n == nouvelle ligne  
\r ==  retour chariot  
\t == tabulation  

### Usage
[src](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String#instance_methods)   