# Javascript 

## Définition
* Script interprété est executé coté client ( browser ) 
* Anime les pages et améliore l'interactivité avec l'utilisateur
* Langue object ( +- support l'objet mais nativement prototype [src](https://developer.mozilla.org/en-US/docs/Web/JavaScript/About_JavaScript)

## Non usage
* Navigateur rare ne supportant pas bien le dom 
* Navigateur n'utilisant pas js ou js desacité ( sic )

## Outils
Moteur [V8](https://en.wikipedia.org/wiki/V8_(JavaScript_engine)) ou [autre](https://alternativeto.net/software/v8/) ( chrome F12 -> console)

## Utilité
Js permet d'interagir avec le dom.    
Le dom est la structure html d'une page.   
Les interactions peuvent etre de modifier, ajouter, supprimer dans le (dom)[https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction].   
Ou réagir à des actions utilisateurs via les [events](https://developer.mozilla.org/en-US/docs/Web/Events) 

## Inclure
Inclusion au sein du html via la balise [script](https://www.w3schools.com/tags/tag_script.asp).    

Soit depuis un fichier externe ou directement dans la page html.

```html
<!DOCTYPE html>
<html>
<head>
<title>Title of the document</title>
</head>

<body>
The content of the document......

<script>
    console.log('js direct in html')
</script>

<script src="js_from_other_file.js"></script>
</body>

</html>
```

## Syntax

[src](https://www.frontendcheatsheets.com/javascript)

## Basics

* [alert](https://developer.mozilla.org/en-US/docs/Web/API/Window/alert)
* [prompt](https://developer.mozilla.org/en-US/docs/Web/API/Window/prompt)
* [document](https://developer.mozilla.org/en-US/docs/Web/API/Document)
* [document.write](https://developer.mozilla.org/en-US/docs/Web/API/Document/write)

## Types
* [types](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)   
* [casting](https://flaviocopes.com/javascript-casting/)   
* [converstion](https://www.w3schools.com/js/js_type_conversion.asp)
*casting signifie une converstion implicitie ( ou explicite ) du type de la variable*

## Operators
[operators](https://www.w3schools.com/jsref/jsref_operators.asp)

## Variables
[declaration](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements#declarations)