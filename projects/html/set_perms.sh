#!/bin/bash 

ROOT_PATH='./'

sudo chmod 775 -R "${ROOT_PATH}assets/${*}"

sudo chown $USER:www-data -R "${ROOT_PATH}assets/${*}"

exit $?
