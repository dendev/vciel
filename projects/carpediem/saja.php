<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | Le SAJA</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
                <div class="navbar navbar-inverse" role="banner">
                    <div class="container">

                        <?php 
           include('inc/nav.php')
           ?>

                    </div>
                </div>
        </header>
        <!--/#header-->


        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">Le SAJA</h1>
                                <!-- <p>Why our Clients love to work with us.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#page-breadcrumb-->

        <section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="images/photos/dago.jpg" class="margin-bottom right" alt="bénéficiaire préparant des sandwiches">
                        <!-- <h1 class="margin-bottom">Le SAJA</h1> -->
                        <div class="text-justify">
                            
                            <p>
                                Le SAJA, service d’accueil de jour pour adultes est agréé et subventionné par l’AViQ
                                pour 62 personnes et 6 personnes en convention nominative.
                                Il accueille des personnes adultes avec un handicap intellectuel à partir de 18 ans. Le
                                service est ouvert tous les jours de la semaine sauf les we et jours fériés, de 8h30 à
                                16h30.</p>
                                <p>Nous fonctionnons par projet de vie et proposons aux usagers des activités diverses :
                                    sportives, culturelles, culinaires, artistiques… ainsi que l’hippothérapie, les sorties,
                                    les apprentissages et activités cognitives... Toutes font parties de leur quotidien et
                                    tentent de répondre au mieux à leurs envies et leurs besoins.</p>
                                    
                                    <br><br>
                                    <h2>
                                        Groupes d'accueil
                                    </h2>
                                    <p>En construction</p>
                                    <br>
                                    <h2>
                                        Activités
                                    </h2>
                                    <br>
                                    
                                    <ul id="tab2" class="nav nav-pills">
                                        <li class=""><a href="#tab2-item1" data-toggle="tab"
                                        aria-expanded="false">Sport</a></li>
                                        <li class=""><a href="#tab2-item2" data-toggle="tab" aria-expanded="false">Thérapie</a>
                                    </li>
                                    <li class=""><a href="#tab2-item3" data-toggle="tab" aria-expanded="false">Service</a>
                                </li>
                                <li class=""><a href="#tab2-item4" data-toggle="tab" aria-expanded="false">Bien-être</a>
                            </li>
                            <li class=""><a href="#tab2-item5" data-toggle="tab" aria-expanded="false">Art</a></li>
                            <li class=""><a href="#tab2-item6" data-toggle="tab"
                            aria-expanded="false">Apprentissage</a></li>
                            <li class=""><a href="#tab2-item7" data-toggle="tab" aria-expanded="false">Cuisine</a>
                        </li>
                        <li class=""><a href="#tab2-item8" data-toggle="tab"
                        aria-expanded="false">Expression</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab2-item1">
                        <h3>
                            1. Activités sportives
                        </h3>
                        <br>
                        <ul>
                            <li>
                                • <b>Tabora : </b>
                                Nos usagers se rendent deux matinées par semaine au centre sportif de
                                Salzinne pour
                                y réaliser des sports tels que tennis de table, badminton& football. Ces
                                activités
                                visent l’intégration sociale, la mise en mouvement, le développement de
                                l’esprit
                                d’équipe et le dépassement de soi.
                            </li>
                            
                            <li>
                                • <b>Bowling : </b>
                                Deux après-midis par semaine, nos usagers se rendent au Bowling de Saint
                                servais.
                                Via cette
                                activité ils travaillent de manière ludique la socialisation.
                            </li>
                            
                            <li>
                                • <b>Piscines : </b>
                                Nous proposons deux types de piscine durant la semaine, adaptées aux
                                compétences et
                                besoins
                                de nos usagers.
                                
                                - La piscine bien-être vise un travail relaxant des corps, une prise de
                                conscience
                                du schéma
                                            corporel dans un espace de détente et à la mobilisation douce.
                                            - La piscine de Ciney vise quant à elle le développement de l’autonomie de
                                            nos
                                            usagers, la
                                            socialisation, l’entrainement hebdomadaire pour notre participation aux
                                            Spécial
                                            Olympics et
                                            l’atteinte des objectifs fixés annuellement.
                                        </li>
                                        
                                        <li>

                                            • <b>Escalade : </b>
                                            Nos usagers grimpent une fois toutes les deux semaines au mur de la salle
                                            d’escalade
                                            de
                                            Jambes (Be Block). Cette activité tend à développer l’endurance, l’écoute de
                                            soi et
                                            des
                                            autres ainsi que l’ouverture vers d’autres sportifs.
                                        </li>
                                        
                                        <li>
                                            • <b>Sports adaptés : </b>
                                            Nous proposons plusieurs types d’accompagnement sportifs tel que des séances
                                            dans
                                            notre
                                            local équipé de vélos d’appartements, vélo semi couchés et de matériel de
                                            musculation.
                                            Des séances de gymnastique douce sont aussi proposées à nos usagers.
                                            Ces activités sont réalisées dans le but de maintenir nos usagers en forme
                                            et en
                                            bonne
                                            santé.
                                        </li>
                                        
                                        <li>
                                            • <b>VTT : </b>
                                            Durant cette sortie, nos sportifs profitent des bois avoisinants pour
                                            développer
                                            leur
                                            endurance.
                                        </li>
                                        
                                        <li>
                                            • <b>Promenade PMR : </b>
                                            Nous explorons notre belle région namuroise tous les vendredis matin. Cette
                                            promenade permet
                                            une entraide entre nos marcheurs et nos usagers à mobilité réduite.
                                        </li>
                                        
                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-sport-pt.png" alt="Activités sportives chez Carpe Diem" width="100%">
                                    
                                </div>
                                <div class="tab-pane fade active in" id="tab2-item2">
                                    <h3>
                                        2. Activités thérapeutiques.
                                    </h3>
                                    <br>
                                    <ul>
                                        <li>
                                            • <b>Hippothérapie :</b> 
                                            Tous les jeudis nous nous rendons à la ferme Paco’m les autres pour une
                                            séance
                                            d’accompagnement équestre qui vise à apporter à nos usagers un moment de
                                            douceur et
                                            de
                                            bien-être autour des chevaux.
                                        </li>
                                        
                                        <li>
                                            • <b>Activ’dog :</b>
                                            Toutes les deux semaines, Nali, un Golden retriever rend visite à nos
                                            usagers pour
                                            une
                                            séance autour de jeux et d’apprentissages.
                                        </li>
                                        
                                        <li>
                                            • <b>Musicothérapie :</b>
                                            Rachita de Musico Terre Happy vient proposer des séances autour des sons, de
                                            la
                                            musique et
                                            des percussions à nos mélomanes. Ces sessions permettent à nos usagers de
                                            s’exprimer
                                            autour
                                            des mélodies hypnotiques.
                                        </li>
                                        <li>
                                            • <b>Snoezelen :</b>
                                            Les éducatrices formées proposent des accompagnements individuels qui visent
                                            à la
                                            prise de
                                            conscience du corps, à la perception sensorielle et l’expression des
                                            émotions.
                                        </li>
                                        
                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-therapie-pt.png" alt="Activités thérapeutiques chez Carpe Diem" width="100%">
                                    
                                </div>
                                <div class="tab-pane fade active in" id="tab2-item3">
                                    <h3>
                                        
                                        3. Activités de service
                                    </h3>
                                    <br>
                                    <ul>
                                        
                                        <li>
                                            Ces activités ont pour but de réaliser des services pour la collectivité,
                                            d’amener nos
                                            usagers vers une indépendance au sein de notre institution et une ouverture
                                            sur notre
                                            quartier.
                                            Ces objectifs sont réalisés par le biais de plusieurs activités telles que :
                                                - Réalisation de sandwichs pour le personnel
                                                - Repassage du linge des bénéficiaires du srna espace « studios »
                                                - Carwash
                                                - Aide cuisine : Nos usagers ont la responsabilité d’aider à la
                                                collectivité, cela passe
                                                par
                                                la mise des tables, les vaisselles, le réassort des boissons, ….
                                                - Gestion des déchets : nous sensibilisons l’ensemble de l’institution à
                                                l’écologie et
                                                donc
                                                le tri des déchets fait partie de notre philosophie.
                                            </li>
                                            
                                        </ul>
                                        <br>
                                        <img src="images/photos/saja-services-pt.png" alt="Activités de services chez Carpe Diem" width="100%">
                                        
                                    </div>
                                    <div class="tab-pane fade active in" id="tab2-item4">
                                        
                                        <h3>
                                            
                                            4. Activités de bien être
                                        </h3>
                                        <br>
                                        <img class="left" src="images/photos/bain-etre.jpg" alt="Activités de bien être chez Carpe Diem" style="width:175px !important">

                                        <ul>
                                            </li>
                                            <li>
                                                • <b>Relaxation :</b>
                                                Durant ces temps d’activité, de la relaxation guidée est proposée aux
                                                usagers ainsi
                                                que des
                                                séances de détente musculaire après leur semaine sportive.
                                            </li>
                                            <li>
                                                • <b>Bien être femmes :</b>
                                                Lors de cette séance, il est proposé aux usagères des ateliers vernis,
                                                maquillage,
                                                coiffure
                                                et massage
                                            </li>
                                        <li>
                                            • <b>Bien être hommes :</b>
                                            Parce que les hommes aussi ont droit à leur moment de détente durant lequel
                                            ils sont
                                            invités
                                            à prendre soin d’eux, de leurs corps et de leurs apparences à travers le
                                            rasage, des
                                            massages, pédicure, etc.
                                        </li>
                                        <li>
                                            • <b>Bain de pieds :</b>
                                            Libres des leurs chaussures trop serrées, nous accordons du temps aux
                                            usagers mais
                                            surtout à
                                            leurs pieds…
                                            Cela permet d’améliorer la circulation sanguine et de la relancer en cas de
                                            pieds
                                            froids...
                                            Mais surtout, c'est un moment idéal pour s’occuper de soi et de prendre soin
                                            de ses
                                            pieds .
                                        </li>
                                        
                                    </ul>
                                    
                                </div>
                                <div class="tab-pane fade active in" id="tab2-item5">
                                    
                                    <h3>
                                        5. Activités artistiques :
                                    </h3>
                                    <br>
                                    <ul>
                                        <li>
                                            • <b>Peinture sur toile :</b>
                                            ”La peinture vient de l'endroit où les mots ne peuvent plus s'exprimer.”
                                            Par le biais de cet art, nos bénéficiaires peuvent s’ils le souhaitent
                                            trouver un
                                            moyen
                                            alternatif de s’exprimer et mettre en lumière leurs pensées et leurs idées.
                                        </li>
                                        <li>
                                            • <b>Techniques artistiques :</b>
                                            Lors de ces différents ateliers, les participants sont amenés à découvrir et
                                            travailler
                                            diverses techniques artistiques telles que le collage, le cartonnage, la
                                            broderie
                                            sur
                                            papier, le papier mâché ou encore le dessin au fusain.
                                        </li>
                                        <li>
                                            • <b>Mosaïques :</b>
                                            Les usagers participants à cette activité créent nos panneaux de
                                            signalisation internes.

                                        </li>
                                        <li>
                                            • <b>Décoration de saison :</b>
                                            Au fil des saisons, les usagers participent à la réalisation des différentes
                                            décorations
                                            afin de faire vivre nos couloirs et nos lieux de vie communs
                                        </li>
                                        <li>
                                            • <b>Mercerie / Point de croix :</b>
                                            Cette activité permet de maintenir les apprentissages de nos bénéficiaires
                                            les plus
                                            âgés et
                                            nos plus jeunes désirant d’apprendre. En effet nos ainés ayant appris ces
                                            techniques
                                            en
                                            milieu scolaire adorent ces moments.

                                        </li>
                                        <li>
                                            • <b>Zentangle :</b>
                                            Cette méthode permet de développer sa concentration, de se détendre ou
                                            d’améliorer
                                            sa
                                            dextérité. Sans compter qu’une fois maîtrisée, la méthode permet aussi de
                                            créer de
                                            jolis
                                            dessins apaisants.

                                        </li>
                                        <li>

                                            • <b>Art postal :</b>
                                            Création de cartes postale et petits courriers à l’aide d’une technique de
                                            découpage
                                            et de
                                            collages
                                        </li>

                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-art-pt.png" alt="Activités artistiques chez Carpe Diem" width="100%">

                                </div>
                                <div class="tab-pane fade active in" id="tab2-item6">



                                    <h3>

                                        6. Les activités d’apprentissages
                                    </h3>
                                    <br>
                                    <ul>
                                        <li>

                                            • <b>Montessori :</b>
                                            Lors de cette activité, leurs usagers réaliser des apprentissages ou un
                                            maintien de leurs
                                            acquis suivant cette pédagogie scientifique s'appuyant sur une démarche
                                            expérimentale et des
                                            observations.
                                        </li>
                                        <li>

                                            • <b>Découverte :</b>
                                            Pendant cette matinée, les usagers sont amenés à élargir leurs connaissances
                                            tant
                                            culturelles, artistiques que musicales. Selon leurs demandes l’éducateur
                                            leur permet de
                                            découvrir des sujets, des peintures, des chanteurs de leurs choix. Ils sont
                                            invités à amener
                                            le sujet de leur activité.
                                        </li>
                                        <li>

                                            • <b>Ecologie :</b>
                                            Cette activité permet de sensibiliser nos usagers au bien être de notre
                                            planète, à ce que
                                            l’on pourrait mettre en place au quotidien pour améliorer notre impact
                                            écologique.
                                            Concrètement nous réalisons nos produits nous-mêmes tels que de la lessive
                                            liquide, des
                                            tablettes de lave-vaisselle, des sacs à vrac, des bee warps, ect …
                                        </li>
                                        <li>

                                            • <b>Jeux pédagogiques :</b>
                                            Cette activité autour de jeux de société ou de jeux individuels travaille
                                            essentiellement le
                                            maintien des acquis de manière ludique : Nous nous servons de notre armoire
                                            ludothèque
                                            fournies de jeux de coopération, d’association, de réflexion.
                                        </li>
                                        <li>

                                            • <b>Autonomie :</b>
                                            En vue de leurs objectifs choisis avec les usagers dans leur projet
                                            individuel, ceux-ci se
                                            rendent en ville afin de réaliser leurs achats comme tout un chacun.
                                            Ils y apprennent à gérer leur budget, leur liste de courses ainsi que les
                                            codes de la
                                            société.
                                        </li>
                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-apprentissage-pt.png" alt="Activités d'apprentissage chez Carpe Diem" width="100%">

                                </div>
                                <div class="tab-pane fade active in" id="tab2-item7">
                                    <h3>

                                        7. Activités culinaires
                                    </h3>
                                    <br>
                                    <ul>
                                        <li>
                                            • <b>Collation :</b>
                                            Tous les lundis, nos usagers se transforment en boulangers / pâtissiers afin
                                            de cuisiner les
                                            collations distribuées à 15h30. Ils réalisent l’ensemble des étapes, du
                                            choix de la recette
                                            aux courses et en passant par la vaisselle.
                                        </li>

                                        <li>


                                            • <b>Cocktail :</b>
                                            Sans alcool évidemment, nos usagers (limités dans leur dextérité préparent
                                            tous les
                                            mercredis matin de délicieux cocktails que nous dégustons à la collation.
                                            Mélanges de goûts
                                            & saveurs garantis !
                                        </li>

                                        <li>

                                            • <b>Repas :</b>
                                            Comme à la maison nos bénéficiaires cuisinent leur repas de midi et le
                                            déguste autour d’une
                                            table dans une ambiance conviviale.

                                        </li>
                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-cuisine-pt.png" alt="Activités culinaires chez Carpe Diem" width="100%">


                                </div>
                                <div class="tab-pane fade active in" id="tab2-item8">
                                    <h3> 8. Activités d’expressions
                                    </h3>
                                    <br>
                                    <ul>
                                        <li>
                                            • <b>Photos & Vidéos</b>
                                            Nous profitons de cette activité pour fournir les photos et monter des
                                            reportages pour
                                            alimenter notre page Facebook et notre site internet.
                                        </li>
                                        <li>

                                            • <b>Groupes de paroles :</b>
                                            Parce que leur parole est importante nous proposons plusieurs groupes en
                                            fonction de
                                            thématiques différentes telles que l’autonomie, les réseaux sociaux, la
                                            famille, les projets
                                            individuels et le conseil des usagers.
                                        </li>
                                        <li>
                                            • <b>Médias :</b>
                                            En collaboration avec l’activité photos vidéos les usagers participants
                                            créent les
                                            publications pour notre page Facebook.
                                        </li>
                                        <li>
                                            • <b>Théâtre :</b>

                                            Tous les jeudis matin nos acteurs revisite des scènes de film, des faits
                                            historiques mais se
                                            mettent avant tout en scène eux-mêmes. Le théâtre, comme toute pratique
                                            artistique participe
                                            à l’épanouissement de nos usagers. Il peut être un outil précieux de
                                            développement
                                            personnel. Sa particularité par rapport aux autres arts est sa dimension
                                            collective et le
                                            fait que le « matériel » de travail soit l’acteur lui-même.

                                        </li>
                                        <li>
                                            • <b>Karaoké :</b>

                                            De Johnny Halliday en passant par Louane et Frédéric François, le but n’est
                                            pas de chanter
                                            juste mais d’offrir un espace défouloir.
                                        </li>
                                        <li>
                                            • <b>Contes :</b>
                                            Développer une thématique choisie par les participants via le conte,
                                            illustration et travail
                                            de la compréhension de celui-ci.
                                        </li>
                                        
                                    </ul>
                                    <br>
                                    <img src="images/photos/saja-expression-pt.png" alt="Activités d'expression chez Carpe Diem" width="100%">

                                    
                                </div>
                            </div>
                            
                            <br>
                            <br>
                            <h2>
                                Nos partenaires
                            </h2>

                             

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#about-company-->
<br>
        <section id="services">
            <div class="container">


                <div class="row">
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a class="jet-inline-svg jet-inline-svg--custom-width"
                                    href="https://compagnonsbatisseurs.be/" target="_blank">
                                    <img src="images/logos/compagnons-batisseurs.jpg" alt="Compagnons bâtisseurs"
                                        height="70">
                            </div>
                            <h2><a class="jet-inline-svg jet-inline-svg--custom-width"
                                    href="https://compagnonsbatisseurs.be/" target="_blank">Les Compagnons bâtisseurs</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="600ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="http://www.evasionasbl.be/" target="_blank"><img src="images/logos/none.png"
                                        alt="EVASION asbl" height="70"></a>
                            </div>
                            <h2><a href="http://www.evasionasbl.be/" target="_blank">EVASION asbl</a></h2>
                            <p><a href="https://www.facebook.com/evasions.asbl/"> Evasions ASBL | Facebook</a></p>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="http://www.alteoasbl.be/" target="_blank"><img src="images/logos/alteo.jpg"
                                        alt="Alteo" height="70"></a>
                            </div>
                            <h2><a href="http://www.alteoasbl.be/" target="_blank">ALTEO</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="https://www.rocevasion.be/" target="_blank"><img
                                        src="images/logos/rocevasion.png" alt="Roc Evasion" height="70"></a>
                            </div>
                            <h2><a href="https://www.rocevasion.be/" target="_blank">Roc Evasion</a></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="https://www.namur.be/fr/annuaire/centre-namurois-des-sports"
                                    target="_blank"><img src="images/logos/none.png" alt="tabora" height="70"></a>
                            </div>
                            <h2><a href="https://www.namur.be/fr/annuaire/centre-namurois-des-sports"
                                    target="_blank">Salle de sport Tabora</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="600ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="https://www.bowlingnamur.be/" target="_blank"><img
                                        src="images/logos/bowling.jpg" alt="Bowling de Namur" height="70"></a>
                            </div>
                            <h2><a href="https://www.bowlingnamur.be/" target="_blank">Bowling de Namur</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="https://www.ciney.be/annuaire/piscine-communale" target="_blank"><img
                                        src="images/logos/none.png" alt="Piscine Ciney" height="70"></a>
                            </div>
                            <h2><a href="https://www.ciney.be/annuaire/piscine-communale" target="_blank">Piscine communale de Ciney</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="https://fr-fr.facebook.com/pages/category/Dance---Night-Club/moulindesolieres.club.50/posts/"
                                    target="_blank"><img src="images/logos/moulin-solieres.jpg" alt="Moulin de Solières"
                                        height="70"></a>
                            </div>
                            <h2><a href="https://fr-fr.facebook.com/pages/category/Dance---Night-Club/moulindesolieres.club.50/posts/"
                                    target="_blank">Le Moulin de Solières</a></h2>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="" target="_blank"><img src="images/logos/none.png" alt="" height="70"></a>
                            </div>
                            <h2>Musicothérapie</h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="600ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="https://lafermepacomlesautres.be/" target="_blank"><img
                                        src="images/logos/ferme-pacom.webp" alt="Ferme Pac-com les autres"
                                        height="70"></a>
                            </div>
                            <h2><a href="https://lafermepacomlesautres.be/" target="_blank">Ferme Pac’com les autres</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="http://www.activdog.be/" target="_blank"><img src="images/logos/activdog.jpg"
                                        alt="ActivDog" height="70"></a>
                            </div>
                            <h2><a href="http://www.activdog.be/" target="_blank">Activ’Dog</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="https://www.aufildeleau.info/" target="_blank"><img
                                        src="images/logos/aufildeleau.png" alt="Au fil de l'eau" height="70"></a>
                            </div>
                            <h2><a href="https://www.aufildeleau.info/" target="_blank">Au fil de l’eau</a></h2>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="https://www.mateteau.be/" target="_blank"><img src="images/logos/mat&eau.png"
                                        alt="mat'et eau" height="70"></a>
                            </div>
                            <h2> <a href="https://www.mateteau.be/" target="_blank">Mat&eau asbl</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="600ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="http://www.sportadapte.be/" target="_blank"><img src="images/logos/fema.jpg"
                                        alt="FéMA" height="70"></a>
                            </div>
                            <h2><a href="http://www.sportadapte.be/" target="_blank">FéMA</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <!-- <img src="images/logos/icon3.png" alt=""> -->
                                <a href="http://www.ecoledebellevue.com/" target="_blank"><img src="images/logos/none.png"
                                        alt="ecole belle vue" height="70"></a>
                            </div>
                            <h2><a href="http://www.ecoledebellevue.com/" target="_blank">École Belle-Vue</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                            <a href="http://www.sonefa.be/" target="_blank"><img src="images/logos/none.png"
                                        alt="sonefa" height="70"></a>
                            </div>
                            <h2><a href="http://www.sonefa.be/" target="_blank">Crèche Belle-Vue</a></h2>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="https://www.douceur-coteaux-mosans.be/" target="_blank"><img src="images/logos/coteaux.jpg"
                                        alt="La Douceur des Coteaux Mosans" height="70"></a>
                            </div>
                            <h2> <a href="https://www.douceur-coteaux-mosans.be/" target="_blank">La Douceur des Coteaux Mosans</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="600ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="http://www.asblrcr.be/" target="_blank"><img src="images/logos/none.png"
                                        alt="RCR" height="70"></a>
                            </div>
                            <h2><a href="http://www.asblrcr.be/" target="_blank">RCR</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <!-- <img src="images/logos/icon3.png" alt=""> -->
                                <a href="http://www.asbl-sypa.be/" target="_blank"><img src="images/logos/none.png"
                                        alt="Les petites maisons à Courrière" height="70"></a>
                            </div>
                            <h2><a href="http://www.asbl-sypa.be/" target="_blank">Les petites maisons à Courrière</a></h2>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                        data-wow-delay="900ms">
                        <div class="single-service">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                            <a href="" target="_blank"><img src="images/logos/none.png"
                                        alt="" height="70"></a>
                            </div>
                            <!-- <h2><a href="http://www.sonefa.be/" target="_blank">Crèche Belle vue</a></h2> -->
                        </div>
                    </div>
                </div>

                <!-- </div> -->
            </div>
        </section>
        <!--/#services-->

        <!-- Controls -->
        <a class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
        <a class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
        </div>
        </div>
        </div>
        </section>
        <!--/#team-->
        <?php
        include('inc/footer.php')
        ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>

</html>