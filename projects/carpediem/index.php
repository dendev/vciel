<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>
    <!--/head-->

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-inverse" role="banner">
                <div class="container">

                    <?php 
           include('inc/nav.php')
           ?>
                  
                </div>
            </div>
        </header>
        <!--/#header-->

        <section id="home-slider">
            <div class="container">
                <div class="row">
                    <div class="main-slider">
                        <div class="slide-text">
                            <h1>Carpe Diem asbl</h1>
                            <p>Carpe Diem asbl est un lieu d’accueil et d’hébergement pour personnes adultes en
                                situation de handicap intellectuel dans la région de Namur.<br><br>
                                Un service à taille humaine, soucieux du projet de vie de chacun. </p>
                        </div>
      
                        <img src="images/home/slider/slider.jpg" class="slider-house" alt="valeurs">
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>

        </section>
        <!--/#home-slider-->

<br>
        <section id="clients">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.aviq.be/handicap/" target="_blank"><img src="images/logos/AViQ.jpg"
                                        class="img-responsive" alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.namur.be/fr" target="_blank"><img src="images/logos/Ville-de-Namur.webp"
                                        class="img-responsive" alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.remeso.be/" target="_blank"><img src="images/logos/remeso.svg"
                                        class="img-responsive" alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.cap48.be/" target="_blank"><img src="images/logos/cap48.jpg" class="img-responsive"
                                        alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.loterie-nationale.be/" target="_blank"><img src="images/logos/Loterie.png"
                                        class="img-responsive" alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.inclusion-asbl.be/" target="_blank"><img src="images/logos/inclusion.svg"
                                        class="img-responsive" alt="" height="50"></a>
                            </div>
                        </div>
                    </div>

         
                    <div class="col-sm-12">

                        <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">

                            <div class="col-xs-3 col-sm-2 text-center">
                                <a href="https://www.wallonie.be/fr" target="_blank"><img src="images/logos/wallonie_v.png" class="img-responsive" alt=""
                                        width="100"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="https://www.leraq.be/" target="_blank"><img src="images/logos/RAQ.png" class="img-responsive"
                                        alt="" height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="#"><img src="images/logos/none.png" class="img-responsive" alt=""
                                        height="50"></a>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--/#clients-->

        <?php
        include('inc/footer.php')
        ?>
        <!--/#footer-->


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
    </body>

</html>