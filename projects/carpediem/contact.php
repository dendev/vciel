<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | Contact</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>
    <!--/head-->

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-inverse" role="banner">
                <div class="container">

                    <?php 
           include('inc/nav.php')
           ?>
                    
                </div>
            </div>
        </header>
        <!--/#header-->

        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">Pour nous (re)joindre...</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#action-->

        <section id="map-section">
            <div class="container">
                <p><b>Téléphone :</b> 081/31.24.34</p>
                <p><b>E-mail :</b> contact@carpediemasbl.be</p>
                <br>
                <p><b>Adresse :</b> Rue du Plateau, 11 - 5100 Jambes (Namur) - Belgique.</p>
                <br>
                <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://www.openstreetmap.org/export/embed.html?bbox=4.886662960052491%2C50.44871915957476%2C4.890654087066651%2C50.4513288297628&amp;layer=mapnik"
                    style="border: 1px solid black"></iframe><br />
                <small><a href="https://www.openstreetmap.org/#map=18/50.45002/4.88866">Afficher une carte plus
                        grande</a></small>
                <br><br>
      
                <p><b>Nos réseaux sociaux :</b></p>
                <ul class="nav nav-pills">
                    <li><a href="https://www.facebook.com/Carpediemasbljambes/" target="_blank"><i class="fa fa-facebook"></i>
                            Facebook</a></li>
                    <li><a href="https://www.instagram.com/carpediemasbljambes/?hl=fr" target="_blank"><i class="fa fa-instagram"></i>
                            Instagram</a></li>
                    <li><a href=""><i class="fa fa-linkedin" target="_blank"></i> LinkedIn</a></li>
                </ul>

            </div>

        </section>
        <!--/#map-section-->

        <?php
        include('inc/footer.php')
        ?>
        <!--/#footer-->


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
    </body>

</html>