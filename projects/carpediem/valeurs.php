<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil,hébergement,  région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | Valeurs</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
                <div class="navbar navbar-inverse" role="banner">
                    <div class="container">

                        <?php 
           include('inc/nav.php')
           ?>
                       
                    </div>
                </div>
        </header>
        <!--/#header-->


        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">Nos valeurs</h1>
                                <!-- <p>Why our Clients love to work with us.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#page-breadcrumb-->

        <section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="text-center">
                            <p>
                                <img src="images/photos/valeurs.jpg" alt="Valeurs" width="800">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#about-company-->
        <br>
        <?php
        include('inc/footer.php')
        ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>

</html>