<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | À propos</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
                <div class="navbar navbar-inverse" role="banner">
                    <div class="container">

                        <?php 
           include('inc/nav.php')
           ?>
                        
                    </div>
                </div>
        </header>
        <!--/#header-->


        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">À propos</h1>
                                <!-- <p>Why our Clients love to work with us.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#page-breadcrumb-->

        <section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="images/home/slider/slider.jpg" class="margin-bottom" alt="3 amis sur un banc"
                            width='400px'>
                        <!-- <h1 class="margin-bottom">&Agrave; propos de notre organisation</h1> -->
                        <div class="text-justify">

                            <p>
                                Carpe Diem asbl est un lieu d’accueil et d’hébergement pour personnes adultes
                                en situation de handicap intellectuel dans la région de Namur.<br>
                                Un service à taille humaine, soucieux du projet de vie de chacun.
                            </p>
                            <p>Carpe diem asbl est un service d'accueil et d'hébergement pour adultes en situation
                                d'handicap intellectuel. Le SAJA (service d’accueil de jour) initialement appelé « Les
                                Ateliers » a vu le jour en 1975.
                                Il est agréé pour 62 personnes et 6 personnes en convention nominative (agrément
                                spécifique).
                                A la demande des familles vieillissantes, un SRNA (service résidentiel de nuit) est venu
                                compléter l'offre en 2010.
                                Afin de répondre au mieux aux besoin des bénéficiaires, l'asbl a ouvert 8 studios de
                                mise en autonomie en 2020 et ce pour des personnes issues de Carpe Diem ou de
                                l'extérieur.
                                Notre objectif est que les personnes se sentent bien, qu’elles soient épanouies et
                                qu'elles puissent réaliser leur projet de vie.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#about-company-->


        <section id="team">
            <div class="container">
                <div class="row">
                    <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
                        L'équipe</h1>
                    <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Notre équipe
                        est composée de...</p>
                    <div id="team-carousel" class="carousel slide wow fadeIn" data-ride="carousel"
                        data-wow-duration="400ms" data-wow-delay="400ms">
                        <!-- Indicators -->
                        <ol class="carousel-indicators visible-xs">
                            <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#team-carousel" data-slide-to="1"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/1.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/2.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/3.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/1.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/4.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/3.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/2.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="team-single-wrapper">
                                        <div class="team-single">
                                            <div class="person-thumb">
                                                <img src="images/aboutus/1.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="social-profile">
                                                <ul class="nav nav-pills">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="person-info">
                                            <h2>John Doe</h2>
                                            <p>CEO &amp; Developer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                        <a class="right team-carousel-control hidden-xs" href="#team-carousel"
                            data-slide="next">right</a>
                    </div>
                </div>
            </div>
        </section>
        <!--/#team-->
        <?php
        include('inc/footer.php')
        ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>

</html>