<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | Concepts</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
                <div class="navbar navbar-inverse" role="banner">
                    <div class="container">

                        <?php 
           include('inc/nav.php')
           ?>
                       
                    </div>
                </div>
        </header>
        <!--/#header-->


        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">Nos concepts</h1>
                                <!-- <p>Why our Clients love to work with us.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#page-breadcrumb-->

        <section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="images/photos/concepts.jpg" class="margin-bottom right" alt="concepts"
                            width='400px'>
                        <!-- <h1 class="margin-bottom">Les concepts de notre organisation</h1> -->
                        <div class="text-justify">

                            <p>
                                <b>La Valorisation des Rôles Sociaux de Wolf WOLFENSBERGER :</b>
                                Chaque personne à un rôle à jouer au sein de notre société. Cette valorisation passe par
                                l’amélioration de l’image de la personne et par le développement de ses compétences.
                                La qualité de vie de Robert SHALOCK la qualité de vie d’une personne est bonne lorsque
                                ses besoins de base sont rencontrés et qu’elle a les mêmes chances que n’importe qui
                                d’autre de poursuivre et de réaliser ses objectifs dans les différents milieux de vie.
                            </p>
                            <p>
                                <b>L’autodétermination :</b> la possibilité de pouvoir faire des choix et prendre des
                                décisions en accord avec ses préférences, ses valeurs et ses objectifs. Il s’agit,
                                également, d’un apprentissage via l’expérimentation, les erreurs, la prise de risques
                                positifs mesurés et ce tout au long de la vie.
                            </p>
                            <p>

                                <b>L’inclusion :</b> Il s’agit de donner sa place de citoyen à une personne en situation
                                d’handicap. L’inclusion vise aussi à lever les obstacles à l’accessibilité pour tous aux
                                structures ordinaires d’enseignement, de santé, d’emploi, de services sociaux, de
                                loisirs, etc.
                            </p>
                            <p>

                                <b>L’intergénérationnel :</b> l’objectif est de renforcer les liens entre les
                                générations à travers le partage d’activités communes, qu’il s’agisse de personnes âgés,
                                d’enfants, d’adolescents, d’adultes sans tenir compte de leurs origines ou de leurs
                                situations.
                            </p>
                            <p>

                                <b>Le Bien vivre ensemble :</b> favoriser la rencontre entre des personnes vivant dans
                                le même quartier autour d’initiatives culturelles, festives ou de toute autre nature
                                favorisant les échanges, la solidarité, la coopération, l’entraide dans un climat
                                bienveillant et respectueux de nos diversités sociales et culturelles.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#about-company-->

        <?php
        include('inc/footer.php')
        ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>

</html>