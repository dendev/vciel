<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Lieu d’accueil et d’hébergement pour personnes adultes en situation de handicap intellectuel à Namur.">
        <meta name="keywords" content="handicap, namur, accueil, hébergement, région namuroise, adultes">
        <meta name="author" content="Formatux">
        <title>Carpe Diem asbl | Le SRNA</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.png">
    </head>

    <body>
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow">
                        <?php
                        include('inc/social.php')
                        ?>
                    </div>
                </div>
                <div class="navbar navbar-inverse" role="banner">
                    <div class="container">

                        <?php 
           include('inc/nav.php')
           ?>
                        
                    </div>
                </div>
        </header>
        <!--/#header-->


        <section id="page-breadcrumb">
            <div class="vertical-center sun">
                <div class="container">
                    <div class="row">
                        <div class="action">
                            <div class="col-sm-12">
                                <h1 class="title">Le SRNA</h1>
                                <!-- <p>Why our Clients love to work with us.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#page-breadcrumb-->

        <section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="images/photos/srna.jpg" id="srna" class="margin-bottom left" alt="4 bénéficiaires">
                        <!-- <h1 class="margin-bottom">Le SRNA</h1> -->
                        <div class="text-justify">

                            <p>
                                Le SRNA, service résidentiel de nuit pour adultes (service SAPS) est agréé par l’AViQ.
                                Il accueille 34 personnes à partir de 18 ans. Le service se veut complémentaire à une
                                activité de jour (centre de jour, travail, bénévolat).</p>
                                <p>Nous souhaitons que chaque personne puisse réaliser son projet de vie, que ce soit dans
                                la partie hébergement « chambres » ou la partie « studios » qui vient d’être ouverte en
                                2020. Nous essayons d’adapter au mieux notre offre de service aux souhaits et
                                compétences de chacun en individualisant au maximum notre accompagnement.</p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#services-->
        <br>

        <!-- Controls -->
        <a class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
        <a class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
        </div>
        </div>
        </div>
        </section>
        <!--/#team-->
        <?php
        include('inc/footer.php')
        ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>

</html>