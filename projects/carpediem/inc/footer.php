<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">
                <!-- <img src="#" class="img-responsive inline" alt=""> -->
            </div>
            <div class="col-md-3 col-sm-6">
                <img src="images/QR.jpg" alt="QR code" title="Scannez ce code avec l'appareil photo de votre gsm">
                <br>
                <a href="images/logos/carpediem.jpg" target="_blank">Télécharger le logo de Carpe Diem</a>
            </div>
            <div class="col-md-5 col-sm-7">
                <div class="contact-info bottom">
                    <h2>Contacts</h2>
                    <address>
                        <p>E-mail: <a href="mailto:contact@carpediemasbl.be">contact@carpediemasbl.be</a> </p>
                        <p>Téléphone: 081/31.24.34</p>
                    </address>

                    <h2>Adresse</h2>
                    <address>
                        Rue du Plateau, 11<br>
                        5100 Jambes <br>
                        Belgique<br>
                    </address>
                </div>
            </div>
	    <div class="col-md-3 col-sm-12">
		<!--
                <div class="contact-form bottom">
                    <h2>Newsletter</h2>
                    <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" required="required"
                                placeholder="Votre adresse e-mail">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-submit" value="M'inscrire">
                        </div>
                    </form>
		</div>
		-->
            </div>
            <img src="images/photos/dots.png" id="dots" alt="" style="margin-left:60px;">

            <br>
            <br>
            <div class="col-sm-12">
                <div class="copyright-text text-center" style="padding-top:90px">
                    <p>&copy; Carpe Diem asbl - &copy; 2021 - <?php echo date('Y') ?></p>
                    <!-- <p>Designed by <a target="_blank" href="http://www.themeum.com">Themeum</a></p> -->
                    <br>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->
