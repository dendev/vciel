<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand" href="index.php">
        <h1><img src="images/logos/carpediem.svg" alt="logo" width="240"></h1>
    </a>

</div>
<nav class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        <!-- <li class="active"><a href="index.php">Accueil</a></li> -->
        <li class="dropdown"><a href="index.php">&Agrave; propos <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="qui.php">Qui sommes-nous ?</a></li>
                <li><a href="histoire.php">Historique</a></li>
                <li><a href="mission.php">Notre mission</a></li>
                <li><a href="valeurs.php">Valeurs</a></li>
                <li><a href="vision.php">Vision</a></li>
                <li><a href="concepts.php">Concepts</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#">Services <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="saja.php">Le SAJA</a></li>
                <li><a href="srna.php">Le SRNA</a></li>
                <li><a href="projets-transversaux.php">Projets transversaux</a></li>
                <li><a href="evenements.php">Événements</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="dons.php">Soutien <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="dons.php">Dons</a></li>
                <li><a href="legs.php">Legs</a></li>
                <li><a href="benevolat.php">Bénévolat</a></li>
            </ul>
        </li>
        <!-- <li><a href="galerie.php">Galerie</a></li> -->
        </li>
        <li><a href="contact.php">Contact</a></li>
    </ul>

</nav>


<!-- <div class="search">
    <form role="form">
        <i class="fa fa-search"></i>
        <div class="field-toggle">
            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
        </div>
    </form>
</div> -->