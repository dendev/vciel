# Projet JS

> Denis De Vos Vciel 2021 - 2022

## General
L'html sert de structure, sa mise en forme est faite par W3.css.   

Les données dynamique sont fournis par le js quand la page est chargé.   
Elle sont issut d'un itération d'un tableau de références.  

Il est possible de facilement ajouter, modifier retirer des données ( sport, police, couleur, année) depuis c'est tableau.   
On peut imagigner externaliser cette source de données et l'obtenir d'un serveur http via une request js.

## Affichage 
Sur une structure de base en html le javascript va prendre en charge le role de fournisseur de données dynamique et la gestion des intéractions utilisateur.   
L'affichage est quand à lui à charge du html au travers des balises div, form, etc.  
Pour plus d'agrément visuelle l'html est accompagner de css et utilise le micro framework [W3.CSS](https://www.w3schools.com/w3css/default.asp)

Certains petits affichage de debug sont disponible dans la console afin de suivre l'évolution du programme.   
Ils peuvent être vue comme des "chambres de visites" permettant une inspection fine.   
Ils sont accompagnés d'un commentaire debug. Pour une mise en prod il est propre de les commenter eux aussi.

 
## Structure 
Plusieurs function js destiné à remplir les select sont similaires et pourrait être factoriser.  
Cette factorisation risque d'augmenter la généricité et réduire la lisibilité du code. 
Des structures redondantes sont donc présentes pour leur bénéficie de lisibilité et la souplesse qu'elles apportents.   
Le changement d'un html générer peut etre fait individuellement sans impacter les autres.

Sont externalise du code des donnée de "configuration" comme l'id des elements html à manipuler ainsi que les données de références ( tableau des sports, polices, taille, ...).  
Ceci afin de préserver au maxium un séparation entre le code opérant et le code de de configuration.  

## Form
L'accés au données du formulaire sont majoritairement faite directement pas l'acces à l'id de l'input.   
Il serait possible de recourir à des selector différents ou utiliser l'objet FormData.   
Comme les lables html reclament un input et que la plupart des champs en disposent, il aurait été domage de ne pas profiter de leur présence.

## Découpe
Différentes fonctions afin d'avoir un traitement spécialiser rpondant à une problématique précise et pour limiter la complexité du code.   

## Scope
Utilisation différentiel du var et let afin de préserver le scope des fonctions et limiter le risque de collision de nom de variable puisque l'import est un import de js brute et non de module js.
Une amélioration pourrait être apporter en tranformer les variables fixes en const.

