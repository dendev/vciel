/*
Ajoutez en haut de la page le titre JEUX OLYMPIQUES (<h1>)
• Récupérez des informations sur 2 sports. Mettez entitre de niveau 2 les noms des sports que
vous avez choisis. Pour chaque sport ajoutez :
o un texte qui le décrit en alignement justifié
o au moins 2 images
o une liste non ordonnée (balise HTML : <ul></ul>) avec au moins 3 épreuves
o un bouton « Ajouter » qui permet d’ajouter une nouvelle épreuve dans la liste des
épreuves
 */

// config
var set_sport_event_id_1 = 'set-sport-event-1';
var set_sport_event_id_2 = 'set-sport-event-2';

var set_sport_event_list_id_1 = 'set-sport-events-list-1';
var set_sport_event_list_id_2 = 'set-sport-events-list-2';


function add_event_1(e)
{
    e.preventDefault();
    add_event(set_sport_event_id_1, set_sport_event_list_id_1)
}

function add_event_2(e)
{
    e.preventDefault();
    add_event(set_sport_event_id_2, set_sport_event_list_id_2)
}

function add_event(set_sport_event_id, set_sport_event_list_id)
{
    //e.preventDefault();

    // get value
    var set_sport_event = document.getElementById(set_sport_event_id);
    var set_sport_event_value = set_sport_event.value

    // clean value
    set_sport_event.value = '';

    // create new li
    var item = `<li>${set_sport_event_value}</li>`

    // add it in list
    var set_sport_event_list = document.getElementById(set_sport_event_list_id);
    set_sport_event_list.innerHTML += item;
}
