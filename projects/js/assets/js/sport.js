/*
• Nom et Prénom (champs de texte de maximum 60 caractères)
• Date de naissance : 3 listes de type <select> : Année, Mois, Jour (dans cet ordre, éviter d’utiliser
la balise <input type="date" />).
o Quand l’utilisateur choisit un mois, affichez le nombre correspondant d’options (de
jours) dans la liste jour. Exemple : si l’utilisateur choisit Janvier, affichez dans la liste
Jour les valeurs entre 1 et 31. Si l’utilisateur choisit Avril, affichez dans la liste Jour les
valeurs entre 1 et 30, etc.
o Affichez automatiquement, en fonction de l’âge de la personne et du sport, sa
catégorie (voir https://fr.wikipedia.org/wiki/Cat%C3%A9gorie_(sports) ).
• Type de classement : individuel, par équipe (boutons radio)
• Participation : Individuel, Équipe (cases à cocher)
• Sport : liste d’options
• Épreuve : liste d’options (synchroniser les listes Sport et Épreuve : quand l’utilisateur choisit
un sport affichez les épreuves correspondantes)
• Commentaires : une zone de texte de 5 lignes et 25 colonnes
• Un bouton pour l'envoi de données et un autre pour la réinitialisation du formulaire
Vérifiez que tous les champs sont remplis et qu’il y a des boutons radio/cases à
 */

// config
let select_year_id = 'year';
let select_month_id = 'month';
let select_day_id = 'day';
let select_sport_id= 'sports';
let select_event_id = 'events';
let set_category_id = 'category';
let form_sport_id = 'form-sport';

// event
window.onload = (event) => {
    console.log( 'page is loaded');
    init_select_sport();
    init_select_year();
    init_select_month();
    init_select_day();
    init_select_event();
}

// fct
function set_category()
{
    // get datas
    let birthday = get_birthday();
    let sport = document.getElementById(select_sport_id).value;

    // define age
    let birthday_date = new Date(birthday[0], birthday[1], birthday[2]);
    let age = _diff_years(birthday_date);

    // ref : translate age to category
    let ref_kayak = {
        'Trop jeune!' : [0, 8],
        'Poussin' : [9, 10],
        'Benjamin' : [11, 12],
        'Minime' : [13, 14],
        'Cadet' : [15, 16],
        'Junior' : [17, 18],
        'Senior' : [19, 34],
        'Vétéran' : [35, 999],
    }

    let ref_escalade = {
        'Trop jeune!' : [0, 7],
        'Microbe': [8, 9],
        'Poussin': [10, 11],
        'Benjamin-e': [12, 13],
        'Minime': [14, 15],
        'Cadet-te': [16, 17],
        'Junior': [18, 19],
        'Senior': [20, 39],
        'Vétéran-e': [40, 999]
    }

    let ref_basketball = {
        'U7 (Baby basketteur)': [0, 6],
        'U8-U9 (Mini-poussin)': [7, 8],
        'U10-U11 (Poussin)': [9, 10],
        'U12-U13 (Benjamin)': [11, 12],
        'U14-U15 (Minime)': [13, 14],
        'U16-U17 (Cadet)': [15, 16],
        'U18-U19-U20 (Junior)': [17, 19],
        'Senior': [20, 999]
    }

    // set current ref
    let ref;
    if( sport === 'kayak')
        ref = ref_kayak;
    else if( sport === 'escalade' )
        ref = ref_escalade;
    else if( sport === 'basketball')
        ref = ref_basketball
    else
        console.error('Bad sport');

    // get category
    let category = '-';
    let min = 0;
    let max = 0;
    if( typeof ref === 'object' )
    {
        for (const [label, limits] of Object.entries(ref))
        {
            min = limits[0];
            max = limits[1];

            if (age >= min && age <= max)
            {
                category = label;
                break;
            }
        }
    }

    // set value in form
    document.getElementById(set_category_id).value = category;

    return category;
}

function get_birthday()
{
    // value
    let year = document.getElementById(select_year_id).value;
    let month = document.getElementById(select_month_id).value;
    let day = document.getElementById(select_day_id).value;

    // debug
    console.log( '--debug: birthday');
    console.log( year );
    console.log( month );
    console.log( day );

    // pack
    let date = [year, month, day];

    return date;
}

function handle_change_sport()
{
    set_category();
    init_select_event();
}

function handle_change_year()
{
    set_category();
    init_select_day();
}

function handle_change_month()
{
    set_category();
    init_select_day();
}

function handle_change_day()
{
    set_category();
}

function submit_sport(e)
{
    e.preventDefault();

    // get element
    let form = document.getElementById(form_sport_id);
    let formData = new FormData(form);

    // check input fields
    let all_fields_filled = true;
    for (var key of formData.keys())
    {
        if( !formData.get(key) )
        {
            all_fields_filled = false;
            console.error('Missing value for ' + key);
            break;
        }
    }

    // check rule max size // already check by html
    let is_valid_size;
    let is_valid_size_firstname = false;
    let is_valid_size_lastname = false;

    if( formData.get('firstname').length < 60)
        is_valid_size_firstname = true;
    if( formData.get('lastname').length < 60)
        is_valid_size_lastname = true;

    is_valid_size = ( is_valid_size_firstname && is_valid_size_lastname);

    // inform
    let msg = ( all_fields_filled && is_valid_size) ? 'Ok tous les champs sont remplis' : 'Ko certains champs ne sont pas remplit ou dépasse la limite';
    alert(msg);
}

// init
function init_select_year()
{
    // get element
    let select = document.getElementById(select_year_id)

    // init iteration
    let start_at = 1960;
    let end_at = 2022

    // iterate && create html
    let options_html = [];
    for( let i = start_at; i < end_at; i++)
    {
        let option_html = `<option value="${i}">${i}</option>`
        options_html.push(option_html);
    }

    // add options created to select
    select.innerHTML = options_html;
}

function init_select_month()
{
    let select = document.getElementById(select_month_id)

    let months = {
        1: "janvier",
        2: "février",
        3: "mars",
        4: "avril",
        5: "mai",
        6: "juin",
        7: "juillet",
        8: "aout",
        9: "septembre",
        10: "octobre",
        11: "novembre",
        12: "decembre",
    }

    let options_html = [];
    for( const [key, value] of Object.entries(months) )
    {
        let option_html = `<option value="${key}">${value}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;
}

function init_select_day()
{
    // ref
    let nb_days_per_month = {
        1: 31,
        2: 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31,
    };

    // get current year && month
    let year = document.getElementById(select_year_id);
    let year_value = year.value;

    let month = document.getElementById(select_month_id);
    let month_value = month.value;

    // identify type of year && month
    let is_leap_year = ((year_value % 4 === 0) && (year_value % 100 !== 0)) || (year_value % 400 === 0);

    let is_february = ( month_value == 2)

    // debug
    console.log( '--debug leap--')
    console.log( year_value)
    console.log( is_leap_year)

    console.log(month_value)
    console.log(is_february)

    // nb days
    let nb_days;
    if( is_leap_year && is_february)
        nb_days = 29;
    else
        nb_days = nb_days_per_month[month_value]

    // set select
    let select = document.getElementById(select_day_id)

    let options_html = [];
    for( let i = 1; i <= nb_days; i++)
    {
        let option_html = `<option value="${i}">${i}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;

    //
    set_category()
}

function init_select_sport()
{
    let select = document.getElementById(select_sport_id)

    let sport = {
        'kayak' : 'Kayak',
        'escalade': 'Escalade',
        'basketball': 'Basketball'
    }

    let options_html = [];
    for( const [key, value] of Object.entries(sport) )
    {
        let option_html = `<option value="${key}">${value}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;
}

function init_select_event()
{
    // elemant && value
    let select = document.getElementById(select_event_id)
    let sport = document.getElementById(select_sport_id).value;

    // ref
    let ref_kayak = {
        'e1' : 'event 1 kayak',
        'e2' : 'event 2 kayak',
        'e3' : 'event 3 kayak',
    };

    let ref_escalade = {
        'e1' : 'event 1 escalade',
        'e2' : 'event 2 escalade',
        'e3' : 'event 3 escalade',
    };

    let ref_basketball = {
        'e1' : 'event 1 basketball',
        'e2' : 'event 2 basketball',
        'e3' : 'event 3 basketball',
    };

    // get ref for current sport
    let ref;
    if( sport === 'kayak')
        ref = ref_kayak;
    else if( sport === 'escalade' )
        ref = ref_escalade;
    else if( sport === 'basketball')
        ref = ref_basketball
    else
        console.error('Bad sport');

    // set events list
    let options_html = [];
    for( const [key, value] of Object.entries(ref) )
    {
        let option_html = `<option value="${key}">${value}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;
}

// -
function _diff_years(dt)
{
    var now = Date.now()

    var diff =(now - dt.getTime()) / 1000;
    diff /= (60 * 60 * 24);
    return Math.abs(Math.round(diff/365.25));

}