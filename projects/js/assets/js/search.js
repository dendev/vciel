/*
Ajoutez un « moteur de recherche » : un champ de texte dans lequel l’utilisateur peut
introduire un mot et un bouton « Rechercher ». Quand l’utilisateur appuie sur le bouton
« Rechercher » le mot introduit par l’utilisateur est recherché dans la page et le nombre
d’apparitions de ce mot dans la page doit s’afficher.
 */

var input_search_id = 'input-search';

function submit_search(e)
{
    e.preventDefault();

    // get values
    var input = document.getElementById(input_search_id);
    var to_find = input.value;
    var texts = document.body.textContent;

    // fin all match
    var regex = new RegExp("(" + to_find + ")", "ig");
    const found = [...texts.matchAll(regex)];

    // count
    var nb = found.length;

    // inform
    alert(`Search result: '${to_find}' found '${nb}' time(s) in page`)
}
