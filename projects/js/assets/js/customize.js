/*
Ajoutez un mini-formulaire qui permet de modifier des propriétés de mise en forme de la page
Web courante (par exemple la police et la taille de police, etc.).
 */

// config
var select_font_id = 'select-font';
var select_font_size_id = 'select-font-size';
var select_background_color_id = 'select-background-color';
var set_title_id = 'set-title';

var title_id = 'title';

var available_fonts = {
    'arial': 'Arial',
    'verdana': 'Verdana',
    'helvetica': 'Helvetica',
    'tahoma': 'Tahoma',
};

var available_font_sizes = [
    12,
    16,
    20,
    24
];

var available_background_colors = {
    'blue' : 'Blue',
    'red'   : 'Red',
    'green'  : 'Green',
    'white' : 'White'
};

// event
window.onload = (event) => {
    console.log( 'page is loaded');
    init_select_font();
    init_select_font_size();
    init_select_background_color();
}

function submit_customize(e)
{
    e.preventDefault();

    // read
    var select_font = document.getElementById(select_font_id);
    var select_font_value = select_font.value;

    var select_font_size = document.getElementById(select_font_size_id);
    var select_font_size_value = select_font_size.value;

    var select_background_color = document.getElementById(select_background_color_id);
    var select_background_color_value = select_background_color.value;

    var set_title = document.getElementById(set_title_id);
    var set_title_value = set_title.value

    // default value
    set_title_value = set_title_value ? set_title_value : 'Project JS'; // ignore empty title

    // debug
    console.log( select_font_value);
    console.log( select_font_size_value);
    console.log( select_background_color_value);
    console.log( set_title_value);

    // do
    document.body.style.fontFamily = select_font_value;
    document.body.style.fontSize = select_font_size_value + 'px';
    document.body.style.backgroundColor = select_background_color_value;
    document.getElementById('title').textContent = set_title_value;
}


// init
function init_select_font()
{
    // get element
    let select = document.getElementById(select_font_id)

    // make html
    let options_html = [];
    for( const [key, value] of Object.entries(available_fonts) )
    {
        let option_html = `<option value="${key}">${value}</option>`
        options_html.push(option_html);
    }

    // add it to element
    select.innerHTML = options_html;
}

function init_select_font_size()
{
    let select = document.getElementById(select_font_size_id)

    let options_html = [];
    for( let i = 0; i < available_font_sizes.length; i++)
    {
        let value = available_font_sizes[i];
        let option_html = `<option value="${value}">${value}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;
}

function init_select_background_color()
{
    let select = document.getElementById(select_background_color_id)

    let options_html = [];
    for( const [key, value] of Object.entries(available_background_colors) )
    {
        let option_html = `<option value="${key}">${value}</option>`
        options_html.push(option_html);
    }
    select.innerHTML = options_html;
}



