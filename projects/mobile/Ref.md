# Ref

> infos et tuto pour la prise en main de swift et dev ios


* [offical - en](https://developer.apple.com/tutorials/app-dev-training/creating-a-card-view)
* [Like Instagrame - fr](https://www.youtube.com/watch?v=hK1uiN0peGk)
* [TableView - fr](https://www.youtube.com/watch?v=kXocJy18p0s)
* [Cheat Sheet Swift - en](https://kpbp.github.io/swiftcheatsheet/)
* [Base swift - fr](https://www.youtube.com/watch?v=iVt3E1b2c7Y)
* [Base swift ( autre ) - fr](https://www.youtube.com/watch?v=wkylArQpz_Y)
