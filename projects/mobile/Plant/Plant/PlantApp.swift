//
//  PlantApp.swift
//  Plant
//
//  Created by Denis De Vos on 27/12/2021.
//

import SwiftUI

@main
struct PlantApp: App {
    @StateObject private var modelData = ModelData()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
