//
//  FamilyHome.swift
//  Plant
//
//  Created by Denis De Vos on 28/12/2021.
//

import SwiftUI

struct FamilyHome: View {
    @EnvironmentObject var modelData: ModelData

    var body: some View {
        NavigationView {
            List {
                
                modelData.features[0].image
                                 .resizable()
                                 .scaledToFill()
                                 .frame(height: 200)
                                 .clipped()
                                 .listRowInsets(EdgeInsets())
                
                ForEach(modelData.families.keys.sorted(), id: \.self) { key in
                    FamilyRow(familyName: key, items: modelData.families[key]!)
                }
                .listRowInsets(EdgeInsets())
            }
            .navigationTitle("Accueil")
        }
    }
}

struct FamilyHome_Previews: PreviewProvider {
    static var previews: some View {
        FamilyHome()
            .environmentObject(ModelData())
    }
}
