//
//  VocabularyList.swift
//  Plant
//
//  Created by Denis De Vos on 27/12/2021.
//

import SwiftUI

struct VocabularyList: View {
    @EnvironmentObject var modelData: ModelData
    
    var body: some View {
        NavigationView {
            List(modelData.vocabularies) { vocabulary in
                NavigationLink {
                    VocabularyDetail(vocabulary: vocabulary)
                    
                }   label: {
                    VocabularyRow(vocabulary: vocabulary)
                }
            }
            .navigationTitle("Vocabulaire")
        }
    }
}

struct VocabularyList_Previews: PreviewProvider {
    static var previews: some View {
        VocabularyList()
            .environmentObject(ModelData())
    }
}
