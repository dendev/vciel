//
//  ContentView.swift
//  Plant
//
//  Created by Denis De Vos on 27/12/2021.
//

import SwiftUI

struct ContentView: View {
    @State private var selection : Tab = .home

    enum Tab {
        case plants
        case home
        case vocabularies
    }
    
    var body: some View {
        TabView(selection: $selection) {
            PlantList()
                .tabItem {
                    Label("Plantes", systemImage: "leaf")
                }
                .tag(Tab.plants)
            
            FamilyHome()
                .tabItem {
                    Label("Familles", systemImage: "house")
                }
                .tag(Tab.home)
            
            VocabularyList()
                .tabItem {
                    Label("Vocabulaire", systemImage: "character.book.closed")
                }
                .tag(Tab.vocabularies)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .environmentObject(ModelData())
            .previewInterfaceOrientation(.portraitUpsideDown)
        }
    }
}
