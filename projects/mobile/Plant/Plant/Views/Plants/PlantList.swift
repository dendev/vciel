//
//  PlantList.swift
//  Plant
//
//  Created by Denis De Vos on 27/12/2021.
//

import SwiftUI

struct PlantList: View {
    @EnvironmentObject var modelData: ModelData
    @State private var showFavouritesOnly = false

    var filteredPlants: [Plant] {
        modelData.plants.filter { plant in
            (!showFavouritesOnly || plant.isFavourite)
        }
    }
    
    var body: some View {
        NavigationView {
            List {
                Toggle(isOn: $showFavouritesOnly){
                    Text("Favoris")
                }
                
                ForEach(filteredPlants) { plant in
                    NavigationLink {
                        PlantDetail(plant: plant)
                    } label: {
                        PlantRow(plant: plant)
                    }
                }
            }
            .navigationTitle("Plantes")
        }
    }
}

struct PlantList_Previews: PreviewProvider {
    static var previews: some View {
        PlantList()
            .environmentObject(ModelData())
    }
}
