//
//  Vocabulary.swift
//  Plant
//
//  Created by Denis De Vos on 27/12/2021.
//

import Foundation

struct Vocabulary: Hashable, Codable, Identifiable {
    var id: Int
    var label: String
    var description: String
}
